# fonner.gitlab.io / fonner.dev

This is the site for fonner.gitlab.io which is also hosted at [fonner.dev](https://fonner.dev/).

It is powered by [Gitlab Pages](https://gitlab.com/fonner/fonner.gitlab.io/pages).

## Developing

Install dependencies with `npm install` (or `pnpm install` or `yarn`).

Start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

## Deployment

Push to Gitlab and a [Gitlab pipeline](https://gitlab.com/fonner/fonner.gitlab.io/-/pipelines) will build and deploy the site to [Pages](https://gitlab.com/fonner/fonner.gitlab.io/pages).
